﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Controllers.Models
{
    public class Geometry
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public double GetSquare() => Width * Heigth / 2;
    }
}
