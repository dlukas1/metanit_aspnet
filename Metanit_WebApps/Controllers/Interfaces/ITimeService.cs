﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Controllers.Interfaces
{
    public interface ITimeService
    {
        string Time { get; }
    }
}
