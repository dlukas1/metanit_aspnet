﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Controllers.Interfaces;
using Controllers.Models;
using Controllers.Util;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Controllers.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITimeService _timeService;
        public HomeController(ITimeService timeService)
        {
            _timeService = timeService;
        }

        /*
         Одно действие Login() расщеплено на два метода: 
         GET-версию, которая отдает представление с формой ввода, 
         и POST-версию, которая принимает введенные в эту форму данные.
        */
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string login, string pwd, bool isMarried, string [] phone)
        {
            string phns = "";
            foreach(string s in phone)
            {
                phns = phns + s + " ";
               // phns += ";";
            }

            string authData = $"Login: {login}, Password: {pwd}, is married: {isMarried}, have a {phns} phone(s)";
            return Content(authData);
        }



        //home/time
        public string time()
        {
            return _timeService.Time;
        }

        public IActionResult GetMessage()
        {
            return PartialView("_GetMessage");
        }

        public IActionResult About()
        {
            ViewBag.Countries = new List<string> { "Brazil", "Argenitna",
            "Uruguay", "Chile"}; //В метод View передается список, поэтому моделью представления About.cshtml будет тип List<string>
            return View();
        }

        public IActionResult ViewMudel()
        {
            List<string> countries = new List<string>
            {
                "Brazil", "Argenitna","Uruguay", "Chile"
            };
            return View(countries);
        }

        public string MyTime()
        {
            return _timeService.Time;
        }

        public JsonResult GetName()
        {
            string name = "Tom";
            return Json(name);
        }


    public JsonResult GetUser()
        {
            User user = new User { Name = "Bill", Age = 28 };
            return Json(user);
        }

        public HtmlResult GetHtml()
        {
            return new HtmlResult("<h2> Hello Asp.Net Core! </h2>");
        }


        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public string Square(Geometry geometry)
        {
            return $"Square of triangle with width {geometry.Width} and heigth {geometry.Heigth}" +
                $" is {geometry.GetSquare()}";
        }
    }
}
