﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FilterApp.Models;
using FilterApp.DAL;
using Microsoft.EntityFrameworkCore;
using FilterApp.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FilterApp.Controllers
{
    public class HomeController : Controller
    {
        UserContext db;
        public HomeController(UserContext ctx)
        {
            db = ctx;
        }


        public IActionResult Index(int? company, string name)
        {
            IQueryable<User> users = db.Users.Include(p => p.Company);
            if(company != null && company != 0)
            {
                users = users.Where(p => p.CompanyId == company);
            }
            if (!String.IsNullOrEmpty(name))
            {
                users = users.Where(p => p.Name.Contains(name));
            }

            List<Company> companies = db.Companies.ToList();

            // устанавливаем начальный элемент, который позволит выбрать всех
            companies.Insert(0, new Company { Name = "All", Id = 0 });

            UserListViewModel viewModel = new UserListViewModel
            {
                Users = users.ToList(),
                Companies = new SelectList(companies, "Id", "Name"),
                Name = name
            };
      
            return View(viewModel);
        }


        //PAGINATION
        public async Task<IActionResult> Pagination(int page = 1)
        {
            int pageSize = 3; //num of elements per page
            IQueryable<User> source = db.Users.Include(x => x.Company);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize)
                .Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                Users = items
            };
            return View(viewModel);
        }

        
        
    }
}
