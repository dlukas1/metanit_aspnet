﻿using FilterApp.Models;
using System;
using System.Collections.Generic;


namespace FilterApp.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
