﻿using LinkTagHelpers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkTagHelpers.ViewModels
{
    public class DayTimeViewModel
    {
        public DayTime Period { get; set; }
    }
}
