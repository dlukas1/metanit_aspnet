﻿using System.Collections.Generic;
using System.Linq;
using LinkTagHelpers.Models;
using LinkTagHelpers.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LinkTagHelpers.Controllers
{
    public class HomeController : Controller
    {
        IEnumerable<Company> companies = new List<Company>
        {
            new Company{Id = 1, Name = "Apple"},
            new Company{Id = 2, Name = "Samsung"},
            new Company{Id = 3, Name = "Microsoft"}
        };

        public IActionResult Create()
        {
            ViewBag.Companies = new SelectList(companies, "Id", "Name");
            return View();
        }

        [HttpPost]
        public string Create(Phone phone)
        {
            Company company = companies.FirstOrDefault(c => c.Id == phone.CompanyId);
            return $"Added new element: {phone.Name} ({company?.Name})";
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(DayTimeViewModel model)
        {
            return Content(model.Period.ToString());
        }
    }
}
