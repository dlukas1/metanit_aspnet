﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace LinkTagHelpers.TagHelpers
{
    public class VKTagHelper : TagHelper
    {
        private const string address = "https://vk.com/metanit";
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";// заменяет тег <vk> тегом <a>
                                 // присваивает атрибуту href значение из address
            output.Attributes.SetAttribute("href", address);
            output.Content.SetContent("Public in VK");
        }
    }
}
