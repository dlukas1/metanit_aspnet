﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using RolesAuthApp.Models;

namespace RolesAuthApp.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(Roles = "admin, user")]
        public IActionResult Index()
        {
            string role = User.FindFirst(x =>
            x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;

            return Content($"Your role is {role}");
        }

        [Authorize(Roles ="admin")]
        public IActionResult Admin()
        {
            ViewData["Message"] = "ADMIN AREA";
            return View();
        }
        
    }
}
