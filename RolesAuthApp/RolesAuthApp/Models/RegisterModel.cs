﻿using System.ComponentModel.DataAnnotations;

namespace RolesAuthApp.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="No email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="No password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage ="Invalid password")]
        public string ConfirmPassword { get; set; }
    }
}
