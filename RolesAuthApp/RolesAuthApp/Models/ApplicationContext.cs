﻿using Microsoft.EntityFrameworkCore;

namespace RolesAuthApp.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext>options)
            : base(options) { }
    }
}
