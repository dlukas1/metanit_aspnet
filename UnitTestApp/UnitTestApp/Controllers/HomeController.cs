﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UnitTestApp.Models;

namespace UnitTestApp.Controllers
{
    public class HomeController : Controller
    {
        IRepository repository;
        public HomeController(IRepository r)
        {
            repository = r;
        }



        public IActionResult Index()
        {
            //ViewData["Message"] = "Hello test!";
            //return View("Index");
            return View(repository.GetAllPhones());
        }

        public IActionResult GetPhone(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Phone phone = repository.GetPhone(id.Value);
            if (phone == null)
                return NotFound();
            return View(phone);
        }

        public IActionResult AddPhone() => View();

        [HttpPost]
        public IActionResult AddPhone(Phone phone)
        {
            if (ModelState.IsValid)
            {
                repository.Create(phone);
                return RedirectToAction("Index");
            }
            return View(phone);
        }



        
    }
}
