﻿using System.ComponentModel.DataAnnotations;

namespace AuthCookieApp.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="Enter email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Enter password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage ="Invalid password")]
        public string ConfirmPassword { get; set; }
    }
}
