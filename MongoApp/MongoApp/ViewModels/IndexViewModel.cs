﻿using MongoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//модель представляет данные, выводимые на главную страницу
namespace MongoApp.ViewModels
{
    public class IndexViewModel
    {
        public FilterViewModel Filter { get; set; }
        public IEnumerable<Phone> Phones { get; set; }
    }
}
