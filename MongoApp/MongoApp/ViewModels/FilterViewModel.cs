﻿

namespace MongoApp.ViewModels
{
    /*
     Так как метод GetPhones класса MobileContext принимает 
     три критерия фильтрации: минимальную и максимальную цену, 
     а также название модели, то класс FilterViewModel принимает 
     три соответствующих свойства.
     */
    public class FilterViewModel
    {
        public string Name { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }
    }
}
