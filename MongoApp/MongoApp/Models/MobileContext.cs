﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

/*
Здесь представлены все стандартные методы CRUD, а также методы управления 
изображениями. Ключевыми объектами в MobileContext являются IMongoDatabase 
(представляет базу данных) и IGridFSBucket (представляет файловое хранилище 
GridFS).
База данных в MongoDB состоит из коллекций. Все объекты Phone будут 
размещатся в коллекции Phones. Для удобного доступа к этой коллекции в 
классе определено одноименное приватное свойство Phones, которое 
представляет объект IMongoCollection.
*/


namespace MongoApp.Models
{
    public class MobileContext
    {
        IMongoDatabase database;    //database
        IGridFSBucket gridFS;       //file storeage

        public MobileContext()
        {
            string connectionString = "mongodb://localhost:27017/mobileStore";
            var connection = new MongoUrlBuilder(connectionString);
            // получаем клиента для взаимодействия с базой данных
            MongoClient client = new MongoClient(connectionString);
            // получаем доступ к самой базе данных
            database = client.GetDatabase(connection.DatabaseName);
            // получаем доступ к файловому хранилищу
            gridFS = new GridFSBucket(database);
        }

        // обращаемся к коллекции Phones
        private IMongoCollection<Phone> Phones
        {
            get { return database.GetCollection<Phone>("Phones"); }
        }

        // получаем все документы, используя критерии фальтрации
        public async Task<IEnumerable<Phone>> GetPhones(int? minPrice, int? maxPrice, string name)
        {
            //filter builder
            var builder = new FilterDefinitionBuilder<Phone>();
            var filter = builder.Empty;

            //name filter
            if (!String.IsNullOrWhiteSpace(name))
            {
                filter = filter & builder.Regex("Name", new BsonRegularExpression(name));
                //System.Diagnostics.Debug.WriteLine(filter.ToString());
               // System.Diagnostics.Debug.WriteLine("OUTPUT PLEASE");
                /*
                if (filter.ToString().
                {
                    filter = filter & builder.Regex("Company", new BsonRegularExpression(name));
                }
                */
            }
            //min price filter
            if (minPrice.HasValue)
            {
                filter = filter & builder.Gte("Price", minPrice.Value);
            }
            //max price filter
            if (maxPrice.HasValue)
            {
                filter = filter & builder.Lte("Price", maxPrice.Value);
            }

            return await Phones.Find(filter).ToListAsync();
        }

        // получаем один документ по id
        public async Task<Phone> GetPhone(string id)
        {
            return await Phones.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        // добавление документа
        public async Task Create(Phone p)
        {
            await Phones.InsertOneAsync(p);
        }

        // обновление документа
        public async Task Update (Phone p)
        {
            await Phones.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(p.Id)), p);
        }

        // удаление документа
        public async Task Remove(string id)
        {
            await Phones.DeleteOneAsync(new BsonDocument("_id", new ObjectId(id)));
        }

        // получение изображения
        public async Task<byte[]> GetImage(string id)
        {
            return await gridFS.DownloadAsBytesAsync(new ObjectId(id));
        }

        // сохранение изображения
        public async Task StoreImage(string id, Stream imageStream, string imageName)
        {
            Phone p = await GetPhone(id);
            if (p.HasImage())
            {
                //if image exist - remove it
                await gridFS.DeleteAsync(new ObjectId(p.ImageId));
            }
            //save image
            ObjectId imageId = await gridFS.UploadFromStreamAsync(imageName, imageStream);

            //update document
            p.ImageId = imageId.ToString();
            var filter = Builders<Phone>.Filter.Eq("_id", new ObjectId(p.Id));
            var update = Builders<Phone>.Update.Set("ImageId", p.ImageId);
            await Phones.UpdateOneAsync(filter, update);

        }
    }
}
