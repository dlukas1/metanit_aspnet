using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPagesApp.Pages
{
    public class FactorialModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet(int number)
        {
            Message = "Enter number";
        }

        public void OnPost(int? number)
        {
            if (number == null || number < 1)
            {
                Message = "Invalid number";
            }
            else
            {
                int result = 1;
                for (int i = 1; i <= number; i++)
                {
                    result *= i;
                }
                Message = $"Number {number} factorial is {result}";
            }
        }
    }
}