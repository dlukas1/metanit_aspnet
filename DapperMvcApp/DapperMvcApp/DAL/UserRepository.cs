﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DapperMvcApp.Models;

namespace DapperMvcApp.DAL
{
    public class UserRepository : IUserRepository
    {
        //SETUP connection string
        string connectionString = null;
        public UserRepository(string conn)
        {
            connectionString = conn;
        }

        public void Create(User user)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = 
                    "INSERT INTO Users (Name,Age) VALUES (@Name, @Age)";
                db.Execute(sqlQuery, user);
                // если мы хотим получить id добавленного пользователя
                //var sqlQuery = "INSERT INTO Users (Name, Age) VALUES(@Name, @Age); SELECT CAST(SCOPE_IDENTITY() as int)";
                //int? userId = db.Query<int>(sqlQuery, user).FirstOrDefault();
                //user.Id = userId.Value;
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Users WHERE Id = @Id";
                db.Execute(sqlQuery, new { id });
            }
        }

        public List<User> GetAllUsers()
        {
            using(IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<User>("SELECT * FROM Users").ToList();
            }
        }

        public User GetUserById(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<User>
                    ("SELECT * FROM Users WHERE id = @id", 
                    new { id }).FirstOrDefault();
            }
        }

        public void Update(User user)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Users SET Name = @Name, Age = @Age" +
                     "WHERE Id = @Id";
                db.Execute(sqlQuery, user);
            }
        }
    }
}
