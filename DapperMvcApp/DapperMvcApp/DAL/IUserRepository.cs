﻿using DapperMvcApp.Models;
using System.Collections.Generic;


namespace DapperMvcApp.DAL
{
    public interface IUserRepository
    {
        void Create(User user);
        void Delete(int id);
        User GetUserById(int id);
        List<User> GetAllUsers();
        void Update(User user);
    }
}
