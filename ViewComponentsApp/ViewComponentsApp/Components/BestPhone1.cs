﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using ViewComponentsApp.Models;

namespace ViewComponentsApp.Components
{
    public class BestPhone1 : ViewComponent
    {
        List<Phone> phones;
        public BestPhone1()
        {
            phones = new List<Phone>
            {
                new Phone {Title="iPhone 7", Price=56000},
                new Phone {Title="Idol S4", Price=26000 },
                new Phone {Title="Elite x3", Price=55000 },
                new Phone {Title="Honor 8", Price=23000 }
            };
        }

        public IViewComponentResult Invoke()
        {
            var item = phones.OrderByDescending(x => x.Price).Take(1).FirstOrDefault();
            return new HtmlContentViewComponentResult(
                new HtmlString($"<h3>Самый дорогой телефон: {item.Title} Цена: {item.Price.ToString("C")}</h3>"));
        }
    }
}
