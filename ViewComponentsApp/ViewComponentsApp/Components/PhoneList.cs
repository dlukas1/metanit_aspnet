﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ViewComponentsApp.Models;

namespace ViewComponentsApp.Components
{
    public class PhoneList : ViewComponent
    {
        IRepository repo;
        public PhoneList(IRepository r)
        {
            repo = r;
        }

        public string Invoke (int maxPrice, int minPrice = 0)
        {
            int count = repo.GetPhones().Count(x => x.Price < maxPrice
            && x.Price > minPrice);
            return $"В диапазоне от {minPrice} до {maxPrice} найдено {count} модели(ей)";
        }
    }
}
