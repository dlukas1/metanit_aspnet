﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewComponentsApp.Models
{
    public interface IRepository
    {
        List<Phone> GetPhones();
    }
}
