﻿using System;
using System.Collections.Generic;
using System.Text;


/*
 Допустим, в случае вывода денег с помощью метода Withdraw 
 нам надо как-то уведомлять об этом самого клиента и, может быть, 
 другие объекты. Для этого создадим делегат AccountStateHandler. 
 Чтобы использовать делегат, нам надо создать переменную этого делегата,
 а затем присвоить ему метод, который будет вызываться делегатом.
     */

namespace Delegates
{
    class Account
    {
        public delegate void AccountStateHandler(string message);
        AccountStateHandler _del;
        public void RegisterHandler(AccountStateHandler del) { _del = del; }

        int _sum;

        public Account(int sum) { _sum = sum; }

        public int CurrentSum { get { return _sum; } }

        public void Put(int sum) { _sum += sum; }

        public void Withdraw(int sum)
        {
            if (sum <_sum)
            {
                _sum -= sum;
                if (_del != null)
                    _del($"Summa {sum} deducted from account");
            }
            else
            {
                if(_del != null)
                {
                    _del("Not enought money!");
                }
            }
        }
    }
}
