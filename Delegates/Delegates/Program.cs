﻿using System;


/*
    
Делегаты представляют такие объекты, которые указывают на методы. 
То есть делегаты - это указатели на методы и с помощью делегатов 
мы можем вызвать данные методы.
Методы, на которые ссылаются делегаты, должны иметь те же параметры и тот же тип возвращаемого значения. Создадим два делегата:

delegate int Operation(int x, int y);
delegate void GetMessage();
Для объявления делегата используется ключевое слово delegate, 
после которого идет возвращаемый тип, название и параметры. 
Первый делегат ссылается на функцию, которая в качестве параметров 
принимает два значения типа int и возвращает некоторое число. Второй делегат 
ссылается на метод без параметров, который ничего не возвращает.
 */

namespace Delegates
{
    class Program
    {
        delegate void GetMessage();// 1. Объявляем делегат
        delegate int Operation(int x, int y);

        private static int Add(int x, int y) => x + y;
        private static int Multiply(int x, int y) => x * y;
        private static void Show_Message(string message)
        {
            Console.WriteLine(message);
        }
        static void Main(string[] args)
        {
            Account account = new Account(200);
            account.RegisterHandler(new Account.AccountStateHandler(Show_Message));
            account.Withdraw(100);
            account.Withdraw(150);
            Console.ReadLine();







            /*
            Operation del = new Operation(Add);
            int result = del.Invoke(4, 5);
            Console.WriteLine(result);

            del = Multiply;
            result = del.Invoke(4, 5);
            Console.WriteLine(result);
            Console.ReadLine();
            */




            /*
            GetMessage del; // 2. Создаем переменную делегата
            if(DateTime.Now.Hour < 12)
            {
                del = GoodMorning; //3.Присваиваем этой переменной адрес метода
            }
            else
            {
                del = GoodEvening;
            }
            del.Invoke();
            Console.ReadLine();
            */
        }

        private static void GoodMorning()
        {
            Console.WriteLine("Good Morning");
        }

        private static void GoodEvening()
        {
            Console.WriteLine("Good evening");
        }
    }
}


