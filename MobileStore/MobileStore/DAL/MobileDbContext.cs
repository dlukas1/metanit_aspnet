﻿using Microsoft.EntityFrameworkCore;
using MobileStore.Models;

namespace MobileStore.DAL
{
    public class MobileDbContext : DbContext
    {
        public DbSet <Phone> Phones { get; set; }
        public DbSet <Order> Orders { get; set; }

        public MobileDbContext(DbContextOptions<MobileDbContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }
    }
}
