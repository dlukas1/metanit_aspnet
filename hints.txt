Source code https://github.com/Apress/pro-asp.net-core-mvc/tree/master/Source%20Code%201-31

EF core install:
1) Install-Package Microsoft.EntityFrameworkCore.SqlServer
2) Install-Package Microsoft.EntityFrameworkCore.Design
3) Install-Package Microsoft.EntityFrameworkCore.Tools

For tests install next packages(only to .test project!)
1) Microsoft.NET.Test.Sdk
2) Moq
3) Xunit
4) NunitTestAdapter
5) xunit.runner.visualstudio

To use sessions install:
1)Microsoft.AspNetCore.Session
2)Microsoft.AspNetCore.Http.Extensions
3)Microsoft.Extensions.Caching.Memory


In appsettings.json:
{
  "ConnectionStrings": 
  {
    "DefaultConnection": "Server=(localdb)\\MSSQLLocalDB;Database=SportsStore;Trusted_Connection=True;MultipleActiveResultSets=true",
    "SportsStoreIdentity": "Server=(localdb)\\MSSQLLocalDB;Database=Identity;Trusted_Connection=True;MultipleActiveResultSets=true"
  }
}


Migrations as follow:
1) Add-Migration initAppDb -Context ApplicationDbContext
2) Update-Database -Context ApplicationDbContext
3) Update-Database -Context AppIdentityDbContext
4) Update-Database -Context AppIdentityDbContext

