﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ClaimsAuthApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClaimsAuthApp.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(Policy ="OnlyForLondon")]
        //[Authorize(Policy = "AgeLimit")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy ="OnlyForMicrosoft")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Authorize(Policy = "AgeLimit")]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Age limit 18.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
