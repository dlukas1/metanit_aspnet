﻿using Microsoft.AspNetCore.Authorization;

namespace ClaimsAuthApp.Models
{
    public class AgeRequirements : IAuthorizationRequirement
    {
        protected internal int Age { get; set; }

        public AgeRequirements(int age)
        {
            Age = age;
        }
    }
}
