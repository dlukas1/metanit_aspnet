﻿using System.ComponentModel.DataAnnotations;

namespace ClaimsAuthApp.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="Enter email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Enter password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage ="Password not matches")]
        public string ConfirmPassword { get; set; }

        public string City { get; set; }
        public string Company { get; set; }
        public int Year { get; set; }
    }
}
