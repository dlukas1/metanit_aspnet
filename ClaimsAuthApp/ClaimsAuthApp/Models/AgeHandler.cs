﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace ClaimsAuthApp.Models
{
    public class AgeHandler : AuthorizationHandler<AgeRequirements>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AgeRequirements requirement)
        {
            if (context.User.HasClaim(c => c.Type == ClaimTypes.DateOfBirth))
            {
                var year = 0;
                if (Int32.TryParse(context.User.FindFirst(c => c.Type == ClaimTypes.DateOfBirth)
                    .Value, out year)) 
                {
                    if (((DateTime.Now.Year - year)>= requirement.Age))
                    {
                        context.Succeed(requirement);
                    }
                }
            }
            return Task.CompletedTask;
        }
    }
}
