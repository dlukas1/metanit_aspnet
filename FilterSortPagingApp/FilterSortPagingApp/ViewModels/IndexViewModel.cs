﻿using FilterSortPagingApp.Models;
using System;
using System.Collections.Generic;

namespace FilterSortPagingApp.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public SortViewModel SortViewModel { get; set; }
    }
}
