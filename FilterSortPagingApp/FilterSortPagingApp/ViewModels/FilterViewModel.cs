﻿using System;
using System.Collections.Generic;
using FilterSortPagingApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FilterSortPagingApp.ViewModels
{
    public class FilterViewModel
    {
        public SelectList Companies { get; set; }
        public int? SelectedCompany { get; set; }
        public string SelectedName { get; set; }
        public FilterViewModel(List<Company> companies, int? company, string name)
        {
            // устанавливаем начальный элемент, который позволит выбрать всех
            companies.Insert(0, new Company { Name = "All", Id = 0 });
            Companies = new SelectList(companies, "Id", "Name", company);
            SelectedCompany = company;
            SelectedName = name;
        }


    }
}
