﻿using Microsoft.EntityFrameworkCore;

namespace FilterSortPagingApp.Models
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public UserContext(DbContextOptions<UserContext> options)
            : base(options) { Database.EnsureCreated(); }

    }
}
