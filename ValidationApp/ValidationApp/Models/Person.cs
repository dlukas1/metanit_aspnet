﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace ValidationApp.Models
{
    [NamePassword] //our custom made attribute
    public class Person
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        //[RegularExpression(@"[A-Zz-z0-9._%+-]+@[Z-za-z0-9.-]+\.[A-za-z]{2,4}", ErrorMessage ="Invalid Email")]
        [EmailAddress]
        [Remote(action:"CheckEmail", controller:"Home", ErrorMessage ="This email is in use")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage ="Passwords not match")]
        public string PasswordConfirm { get; set; }
        [Required]
        [Range(1,110, ErrorMessage ="Invalid age")]
        public int Age { get; set; }
    }
}
