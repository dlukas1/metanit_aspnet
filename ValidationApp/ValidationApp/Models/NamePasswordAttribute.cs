﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ValidationApp.Models
{
    public class NamePasswordAttribute : ValidationAttribute
    {
        public NamePasswordAttribute()
        {
            ErrorMessage = "Name and password should not match";
        }

        public override bool IsValid(object value)
        {
            Person p = value as Person;

            if (p.Name == p.Password)
            {
                return false;
            }
            return true;
        }
    }
}
