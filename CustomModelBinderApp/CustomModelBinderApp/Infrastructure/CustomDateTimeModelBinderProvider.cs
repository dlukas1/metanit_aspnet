﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace CustomModelBinderApp.Infrastructure
{
    public class CustomDateTimeModelBinderProvider : IModelBinderProvider
    {
        private readonly IModelBinder binder =
            new CustomDateTimeModelBinder(new SimpleTypeModelBinder(typeof(DateTime)));
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            return context.Metadata.ModelType == typeof(DateTime) ? binder : null;
        }
    }
}
