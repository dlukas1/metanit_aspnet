﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloWebApi.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Enter name")]
        public string Name { get; set; }
        [Range(1,100, ErrorMessage ="Age between 1 and 100")]
        [Required(ErrorMessage ="Enter age")]
        public int Age { get; set; }
    }
}
