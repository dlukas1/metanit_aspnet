﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using TagHelpersApp.Models;

namespace TagHelpersApp.TagHelpers
{
    public class BookInfoTagHelper : TagHelper
    {
        public Book Book { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            string bookInfoContent = $@"<p>Title: <b>{Book.Title}</b></p>
                <p>Author: <b>{Book.Author}</b></p>
                <p>Year: <b>{Book.Year}</b></p>";

            output.Content.SetHtmlContent(bookInfoContent);
        }
    }
}
