﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Threading.Tasks;
using TagHelpersApp.Models;

namespace TagHelpersApp.TagHelpers
{
    public class VkTagHelper : TagHelper
    {
        /*
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            var target = await output.GetChildContentAsync();
            var content = "<h3>Social networks</h3>" + target.GetContent();
            output.Content.SetHtmlContent(content);
        }*/
        /*
         Здесь с помощью ДИ получаем адрес ид из ссылки 
         и помещаем его в ссылку
         */


        private const string address = "https://vk.com/";
        IHostingEnvironment environment;


        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        public VkTagHelper(IHostingEnvironment env)
        {
            environment = env;
        }
                                           // public LinkInfo info;
                                           // public string Group { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            //получаем из параметров маршрута ид
            string id = ViewContext?.RouteData?.Values["id"]?.ToString();
            if (String.IsNullOrEmpty(id))
            {
                id = "1";
            }
            output.TagName = "a";         
            output.Attributes.SetAttribute("href", address+id);
            if (environment.IsDevelopment())
            {
                output.Attributes.SetAttribute("style", "color:red;");
            }

            
            /* output.Content.SetContent("VK Public");
            output.TagMode = TagMode.StartTagAndEndTag;
            output.PreElement.SetHtmlContent("<h3>Social networks</h3>");
            output.PostElement.SetHtmlContent("<p>Post element</p>");
            */
           // base.Process(context, output);
        }
    }
}
