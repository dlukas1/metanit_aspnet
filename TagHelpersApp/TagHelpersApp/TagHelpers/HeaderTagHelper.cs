﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpersApp.TagHelpers
{
    [HtmlTargetElement(Attributes="header")]
    public class HeaderTagHelper : TagHelper
    {
        /*
         происходит замена существующего элемента на элемент <h2> 
         и удаление атрибута header
             */
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "h2";
            output.Attributes.RemoveAll("header");
        }
    }
}
