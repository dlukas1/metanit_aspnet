﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HtmlHelpers.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HtmlHelpers.Controllers
{
    public class BookController : Controller
    {
        public string Index(string author = "Tolstoy", int id = 1)
        {
            return author + " - " + id.ToString();
        }
    }
}
