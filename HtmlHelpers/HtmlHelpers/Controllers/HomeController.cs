﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HtmlHelpers.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HtmlHelpers.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            List<Phone> phones = new List<Phone>
            {
                new Phone {Id=1, Name="iPhone 7 Pro", Price=680 },
        new Phone {Id=2, Name="Galaxy 7 Edge", Price=640 },
        new Phone {Id=3, Name="HTC 10", Price=500 },
        new Phone {Id=4, Name="Honor 5X", Price=400 }
            };
            ViewBag.Phones = new SelectList(phones, "Id", "Name");
            return View();
        }

        public IActionResult StronglyTyped() => View();

        public IActionResult Details()
        {
            Phone phone = new Phone { Id = 1, Name = "Nexus p8", Price = 220 };
            return View(phone);
        }

        
    }
}
