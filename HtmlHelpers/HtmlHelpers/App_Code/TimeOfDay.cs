﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HtmlHelpers.App_Code
{
    public enum TimeOfDay
    {
        [Display(Name ="Morning")]
        Morning,
        [Display(Name ="Noon")]
        Noon,
        [Display(Name ="Evening")]
        Evening,
        [Display(Name ="Night")]
        Night
    }
}
