﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModelsApp.Models
{
    public class User
    {
        public int Id { get; set; }
        [BindRequired]
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
