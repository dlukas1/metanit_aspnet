﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelsApp.Models;
using ModelsApp.ViewModels;

namespace ModelsApp.Controllers
{
    public class HomeController : Controller
    {
        List<Company> companies;
        List<Phone> phones;

        public HomeController()
        {
            Company apple = new Company { Id = 1, Name = "Apple", Country = "USA" };
            Company microsoft = new Company { Id = 2, Name = "Microsoft", Country = "USA" };
            Company xiaomi = new Company { Id = 3, Name = "Xiaomi", Country = "China" };
            companies = new List<Company> { apple, microsoft, xiaomi };

            phones = new List<Phone>
            {
                new Phone { Id=1, Manufacturer= apple, Name="iPhone 6S", Price=56000 },
                new Phone { Id=2, Manufacturer= apple, Name="iPhone 5S", Price=41000 },
                new Phone { Id=3, Manufacturer= microsoft, Name="Lumia 550", Price=9000 },
                new Phone { Id=4, Manufacturer= microsoft, Name="Lumia 950", Price=40000 },
                new Phone { Id=5, Manufacturer= xiaomi, Name="Nexus 5X", Price=30000 },
                new Phone { Id=6, Manufacturer= xiaomi, Name="Nexus 6P", Price=50000 }
            };
        }

        public IActionResult AddUser() { return View(); }


        [HttpPost]
        public IActionResult AddUser([FromQuery]User user)
        {
            //http://localhost:51583/Home/AddUser?Name=Bob
            //if (ModelState.IsValid)
           // {
                 string userInfo = $"Name: {user.Name} " +
                                $"Age: {user.Age}";
                            return Content(userInfo);
           // }
          //  return Content($"Количество ошибок: {ModelState.ErrorCount}");
        }

        public IActionResult Index(int? companyId)
        {
            // формируем список компаний для передачи в представление
            List<CompanyModel> compModels = companies
                .Select(c => new CompanyModel
                {
                    Id = c.Id,
                    Name = c.Name
                }).ToList();

            compModels.Insert(0, new CompanyModel { Id = 0, Name = "all" });

            IndexViewModel ivm = new IndexViewModel { Companies = compModels, Phones = phones };

            if (companyId != null && companyId > 0)
                ivm.Phones = phones.Where(p => p.Manufacturer.Id == companyId);
            return View(ivm);
        }

        public IActionResult GetPhone(Phone myPhone)
        {
            return Content($"Name: {myPhone.Name}, price: {myPhone.Price}, company: {myPhone.Manufacturer.Name}");
        }
    }
}
